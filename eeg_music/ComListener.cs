﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eeg_music
{
    public class ComListener
    {
        public string PortName { get; set; }

        private SerialPort _serialPort;

        public ComListener(string portName)
        {
            PortName = portName;
            InitPort();

            Console.WriteLine("Press any key to stop");
            Console.WriteLine();
            Console.ReadKey();
            _serialPort.Close();
        }

        private void InitPort()
        {
            _serialPort = new SerialPort(PortName);

            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.RtsEnable = true;

            _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

            _serialPort.Open();
        }

        private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            Console.WriteLine("Data Received:");
            Console.Write(indata);
        }
    }
}
