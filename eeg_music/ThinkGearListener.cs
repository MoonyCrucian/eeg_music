﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroSky.ThinkGear;
using NeuroSky.ThinkGear.Algorithms;
using System.Threading;

namespace eeg_music
{
    public class ThinkGearListener
    {
        public string PortName { get; set; }

        private Connector _connector;
        static byte poorSig;

        public void Test(string portName)
        {
            PortName = portName;
            InitConnection();
            Console.WriteLine("Press any key to stop");
            Console.ReadLine();
            CloseConnection();
        }

        private void InitConnection()
        {
            _connector = new Connector();
            _connector.DeviceConnected += new EventHandler(OnDeviceConnected);
            _connector.DeviceConnectFail += new EventHandler(OnDeviceFail);
            _connector.DeviceValidating += new EventHandler(OnDeviceValidating);

            _connector.ConnectScan(PortName);
        }

        private void CloseConnection()
        {
            if (_connector != null)
                _connector.Close();
            Console.WriteLine("Connection was closed");
        }
        
        static void OnDeviceConnected(object sender, EventArgs e)
        {
            Connector.DeviceEventArgs de = (Connector.DeviceEventArgs)e;
            Console.WriteLine("Device found on: " + de.Device.PortName);
            de.Device.DataReceived += new EventHandler(OnDataReceived);
        }

        
        static void OnDeviceFail(object sender, EventArgs e)
        {
            Console.WriteLine("No devices found");
        }

       
        static void OnDeviceValidating(object sender, EventArgs e)
        {
            Console.WriteLine("Validating");
        }

        static void OnDataReceived(object sender, EventArgs e)
        {
            Device.DataEventArgs de = (Device.DataEventArgs)e;
            DataRow[] tempDataRowArray = de.DataRowArray;

            TGParser tgParser = new TGParser();
            tgParser.Read(de.DataRowArray);

            for (int i = 0; i < tgParser.ParsedData.Length; i++)
            {

                if (tgParser.ParsedData[i].ContainsKey("Raw"))
                {
                    Console.WriteLine("Raw Value:" + tgParser.ParsedData[i]["Raw"]);
                }

                if (tgParser.ParsedData[i].ContainsKey("PoorSignal"))
                {
                    Console.WriteLine("Poor Signal:" + tgParser.ParsedData[i]["PoorSignal"]);

                    poorSig = (byte)tgParser.ParsedData[i]["PoorSignal"];
                }
            }

        }
    }
}
